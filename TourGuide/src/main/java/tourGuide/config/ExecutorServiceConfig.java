package tourGuide.config;

import lombok.AllArgsConstructor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ExecutorService configuration to set up maximum number of threadPools to use
 */
@AllArgsConstructor
public class ExecutorServiceConfig {
    private final int threadPoolSize = 50;
    private final ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);

    public ExecutorService getExecutor() {
       /* ThreadPoolExecutor tourGuideServiceExecutor = (ThreadPoolExecutor) this.executorService;
        System.out.println("Completed tasks: " + tourGuideServiceExecutor.getCompletedTaskCount() +
                "   Active threads: " + tourGuideServiceExecutor.getActiveCount() +
                "   Tasks in queue: " + tourGuideServiceExecutor.getQueue().size());*/
        return executorService;
    }
}
