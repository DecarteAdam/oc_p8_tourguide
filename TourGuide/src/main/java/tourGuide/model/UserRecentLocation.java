package tourGuide.model;

import gpsUtil.location.Location;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRecentLocation {
    private String userId;
    private Location location;
}
