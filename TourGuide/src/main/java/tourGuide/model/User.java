package tourGuide.model;

import gpsUtil.location.VisitedLocation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
@Slf4j
@Data
public class User {
	private final UUID userId;
	private final String userName;
	private String phoneNumber;
	private String emailAddress;
	private Date latestLocationTimestamp;
	private List<VisitedLocation> visitedLocations = new ArrayList<>();
	private List<UserReward> userRewards = new ArrayList<>();
	private UserPreferences userPreferences = new UserPreferences();
	private List<Provider> tripDeals = new ArrayList<>();
	public User(UUID userId, String userName, String phoneNumber, String emailAddress) {
		this.userId = userId;
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
	}

	/**
	 * Add VisitedLocation to User's object
	 * @param visitedLocation to add
	 */
	public void addToVisitedLocations(VisitedLocation visitedLocation) {
		visitedLocations.add(visitedLocation);
	}

	/**
	 * Clear all user's visited locations
	 */
	public void clearVisitedLocations() {
		visitedLocations.clear();
	}

	/**
	 * Add reward to user if the reewards doesn't exist
	 * @param userReward to add
	 */
	public void addUserReward(UserReward userReward) {
		if(userRewards.stream().filter(r -> !r.attraction.equals(userReward.attraction)).count() == 0) {
			userRewards.add(userReward);
			log.info("User with rewards: " + this.userName); //To test getReward endpoint uncomment this line
		}
	}

	/**
	 * Retrive user's last visited location
	 * @return last visited location
	 */
	public VisitedLocation getLastVisitedLocation() {
		return visitedLocations.get(visitedLocations.size() - 1);
	}
}
