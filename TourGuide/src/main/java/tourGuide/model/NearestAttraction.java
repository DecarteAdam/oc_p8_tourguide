package tourGuide.model;

import gpsUtil.location.Location;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NearestAttraction {
    private String attractionName;
    private Location attractionLocation;
    private Location userLocation;
    private long userDistance;
    private int rewardPoints;
}
