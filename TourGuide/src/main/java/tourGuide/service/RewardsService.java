package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.config.ExecutorServiceConfig;
import tourGuide.model.User;
import tourGuide.model.UserReward;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class RewardsService extends ExecutorServiceConfig {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);

	// proximity in miles
    private final int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	private final GpsUtil gpsUtil;
	private final RewardCentral rewardsCentral;

	public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
		logger.debug("-----------RewardsService called---------------");

	}


	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}
	
	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}

	/**
	 * Calculate user rewards based on his visited locations and attractions
	 * @param user to look for
	 */
	public void calculateRewards(User user) {
		List<VisitedLocation> visitedLocations = user.getVisitedLocations();

		CompletableFuture.supplyAsync(gpsUtil::getAttractions, getExecutor())
				.thenAccept(attractions -> {
					for (VisitedLocation visitedLocation : visitedLocations) {
						for (Attraction attraction : attractions) {
							if (user.getUserRewards()
									.stream()
									.noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {
								if (nearAttraction(visitedLocation, attraction)) {
									user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
								}
							}
						}
					}
				});
	}
	
	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > attractionProximityRange ? false : true;
	}
	
	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}
	
	private int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}
	
	public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
        return statuteMiles;
	}

}
