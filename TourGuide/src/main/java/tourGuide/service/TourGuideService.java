package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.config.ExecutorServiceConfig;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.NearestAttraction;
import tourGuide.model.User;
import tourGuide.model.UserRecentLocation;
import tourGuide.model.UserReward;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService extends ExecutorServiceConfig {
    private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    private final GpsUtil gpsUtil;
    private final RewardsService rewardsService;
    private final RewardCentral rewardCentral;
    private final TripPricer tripPricer;
    public final Tracker tracker;
    boolean testMode = true;

    public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService, RewardCentral rewardCentral, TripPricer tripPricer) {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;
        this.rewardCentral = rewardCentral;
        this.tripPricer = tripPricer;

        logger.debug("-----------TourGuideService called---------------");

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }

        tracker = new Tracker(this);

        addShutDownHook();
    }

    /**
     * Retrive user rewards from user
     * @param user who needs to find rewards
     * @return list of user rewards
     */
    public List<UserReward> getUserRewards(User user) {
        return user.getUserRewards();
    }

    /**
     * Retrieve user's visited locations.
     * If user visited locatiosn, return his last location
     * if not, return his actual location
     * @param user who needs to find his location
     * @return visited location
     */
    public VisitedLocation getUserLocation(User user) {
        if (user.getVisitedLocations().size() > 0) {
            return user.getLastVisitedLocation();
        } else {
            return trackUserLocation(user);
        }
    }

    /**
     * Return in-memory stored user
     * @param userName the argument to find with
     * @return user
     */
    public User getUser(String userName) {
        return internalUserMap.get(userName);
    }

    /**
     * Get all in-memory created users
     * @return list of users
     */
    public List<User> getAllUsers() {
        return internalUserMap.values().stream().collect(Collectors.toList());
    }

    /**
     * Add user to in-memory Data Base by it's name if not exist
     * @param user to add
     */
    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    /**
     * Retrieve trip deals for the user
     * @param user who needs trip deals
     * @return list of trip deals
     */
    public List<Provider> getTripDeals(User user) {
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
        List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    /**
     * Track user current location then calculate his rewards
     * @param user to track
     */
    public VisitedLocation trackUserLocation(User user) {
        CompletableFuture.supplyAsync(() -> gpsUtil.getUserLocation(user.getUserId()), getExecutor())
                .thenAccept(user::addToVisitedLocations)
                .thenAccept(v -> {
                    rewardsService.calculateRewards(user);
                });

        return user.getLastVisitedLocation();
    }

    /**
     * Get a list of every user's most recent location
     * @return list of users recent locations
     */
    public List<UserRecentLocation> usersRecentLocation() {
        List<VisitedLocation> userList = getAllUsers()
                .stream()
                .map(this::getUserLocation)
                .collect(Collectors.toList());

        List<UserRecentLocation> userRecentLocationList = new ArrayList<>();
        for (VisitedLocation location : userList) {
            UserRecentLocation userRecentLocation = UserRecentLocation.builder()
                    .userId(location.userId.toString())
                    .location(location.location)
                    .build();

            userRecentLocationList.add(userRecentLocation);
        }

        return userRecentLocationList;
    }

    /**
     * Get the closest five tourist attractions to the user - no matter how far away they are.
     * @param visitedLocation to base for
     * @return list of 5 nearest attractions
     */
    public List<NearestAttraction> getNearByAttractions(VisitedLocation visitedLocation) {
        List<Attraction> nearest5Attractions = gpsUtil.getAttractions()
                .stream()
                .sorted((o1, o2) -> (int) (rewardsService.getDistance(o1, visitedLocation.location) - rewardsService.getDistance(o2, visitedLocation.location)))
                .limit(5)
                .collect(Collectors.toList());

        List<NearestAttraction> nearestAttractionList = new ArrayList<>();

        for (Attraction attraction : nearest5Attractions) {
            NearestAttraction nearestAttraction = NearestAttraction.builder()
                    .attractionName(attraction.attractionName)
                    .attractionLocation(new Location(attraction.latitude, attraction.longitude))
                    .userLocation(visitedLocation.location)
                    .userDistance(Math.round(rewardsService.getDistance(attraction, visitedLocation.location)))
                    .rewardPoints(rewardCentral.getAttractionRewardPoints(attraction.attractionId, visitedLocation.userId))
                    .build();


            nearestAttractionList.add(nearestAttraction);
        }

        return nearestAttractionList;
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                tracker.stopTracking();
            }
        });
    }

    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final Map<String, User> internalUserMap = new HashMap<>();

    private void initializeInternalUsers() {
        logger.debug("-----------initializeInternalUsers method called---------------");
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
