package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.User;
import tourGuide.model.UserReward;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tripPricer.TripPricer;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
public class TestRewardsService {

	private static final GpsUtil gpsUtil = new GpsUtil();;
	private static final RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());


	@Test
	public void userGetRewards() throws InterruptedException {
		Locale.setDefault(Locale.ENGLISH);
		TourGuideService tourGuideService  = new TourGuideService(gpsUtil, rewardsService, new RewardCentral(), new TripPricer());
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		InternalTestHelper.setInternalUserNumber(0);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Attraction attraction = gpsUtil.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
		tourGuideService.trackUserLocation(user);

        //wait trackUserLocation future;
		Thread.sleep(5000);

		List<UserReward> userRewards = user.getUserRewards();
		tourGuideService.tracker.stopTracking();
		assertEquals(1, userRewards.size());
	}
	
	@Test
	public void isWithinAttractionProximity() {
		Attraction attraction = gpsUtil.getAttractions().get(0);
		assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
	}
	
	 // Needs fixed - can throw ConcurrentModificationException
	@Test
	public void nearAllAttractions() throws InterruptedException {
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		InternalTestHelper.setInternalUserNumber(1);
		TourGuideService tourGuideService  = new TourGuideService(gpsUtil, rewardsService, new RewardCentral(), new TripPricer());
		List<User> users = tourGuideService.getAllUsers();

		User user = users.get(0);
		rewardsService.calculateRewards(user);

		Thread.sleep(5000);
		List<UserReward> userRewards = tourGuideService.getUserRewards(user);
		tourGuideService.tracker.stopTracking();

		assertEquals(1, userRewards.size());
	}

}
